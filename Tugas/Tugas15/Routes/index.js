import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { MaterialIcons, AntDesign, Octicons } from '@expo/vector-icons';
import AboutScreen from '../AboutScreen';
import AddScreen from '../AddScreen';
import LoginScreen from '../LoginScreen';
import ProjectScreen from '../ProjectScreen';
import SkillScreen from '../SkillScreen';

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen
      name="Skill"
      component={SkillScreen}
      options={{
        tabBarIcon: ({ color }) => (
          <AntDesign name="linechart" color={color} size={26} />
        ),
      }}
    />
    <Tabs.Screen
      name="Project"
      component={ProjectScreen}
      options={{
        tabBarIcon: ({ color }) => (
          <Octicons name="project" color={color} size={26} />
        ),
      }}
    />
    <Tabs.Screen
      name="Add"
      component={AddScreen}
      options={{
        tabBarIcon: ({ color }) => (
          <AntDesign name="addfile" color={color} size={26} />
        ),
      }}
    />
  </Tabs.Navigator>
);

const DrawerScreen = () => (
  <Drawer.Navigator>
    <Drawer.Screen name="Home" component={TabsScreen} />
    <Drawer.Screen name="About" component={AboutScreen} />
  </Drawer.Navigator>
);

export default () => (
  <AuthStack.Navigator headerMode="none" initialRouteName="LogIn">
    <AuthStack.Screen name="LogIn" component={LoginScreen} />
    <AuthStack.Screen name="MainApp" component={DrawerScreen} />
  </AuthStack.Navigator>
);
