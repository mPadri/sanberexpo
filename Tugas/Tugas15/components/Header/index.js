import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Logo from '../../../Tugas13/assets/images/logo.png';

const Header = ({ title }) => {
  return (
    <View style={styles.wrapHeader}>
      <Image source={Logo} style={{ width: '100%' }} />
      <Text style={styles.titleHeader}>{title}</Text>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  wrapHeader: {
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleHeader: {
    marginTop: 40,
    fontSize: 24,
    fontWeight: 'bold',
    color: '#003366',
  },
});
