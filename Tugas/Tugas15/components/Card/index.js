import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const Card = ({ iconName, skillName, categoryName, percentageProgress }) => {
  return (
    <View style={styles.wrapCard}>
      <View>
        <MaterialCommunityIcons name={iconName} size={75} color="#003366" />
      </View>
      <View style={styles.descCategory}>
        <Text style={{ fontSize: 24, fontWeight: 'bold', color: '#003366' }}>
          {skillName}
        </Text>
        <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#3EC6FF' }}>
          {categoryName}
        </Text>
        <Text
          style={{
            fontSize: 48,
            fontWeight: 'bold',
            textAlign: 'right',
            color: '#fff',
          }}
        >
          {percentageProgress}
        </Text>
      </View>
      <View>
        <Ionicons name="ios-arrow-forward" size={80} color="#003366" />
      </View>
    </View>
  );
};

export default Card;

const styles = StyleSheet.create({
  wrapCard: {
    flexDirection: 'row',
    marginHorizontal: 10,
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    alignItems: 'center',
    backgroundColor: '#B4E9FF',
    borderRadius: 8,
    height: 129,
    elevation: 4,
  },
  descCategory: {
    alignSelf: 'flex-start',
    marginTop: 13,
  },
});
