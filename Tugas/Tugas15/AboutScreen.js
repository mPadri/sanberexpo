import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Constant from 'expo-constants';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import NullPhoto from '../Tugas13/assets/images/null-photo.png';

const AboutScreen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.Header}>
        <Text style={styles.titleHeader}>Tentang Saya</Text>
        <TouchableOpacity>
          <Image
            source={NullPhoto}
            style={{ width: 120, height: 120, paddingHorizontal: 10 }}
          />
        </TouchableOpacity>
        <View style={styles.wraperIdentitas}>
          <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#003366' }}>
            Mukhlis Hanafi
          </Text>
          <Text style={styles.titleProfession}>React Native Developer</Text>
        </View>
      </View>
      <View style={styles.cardPortfolio}>
        <Text style={{ fontWeight: '400', color: '#003366' }}>Portfolio</Text>
        <View
          style={{ borderWidth: 1, marginTop: 5, borderColor: '#8e918f' }}
        />
        <View style={styles.wraperTools}>
          <TouchableOpacity style={{ alignItems: 'center' }}>
            <FontAwesome name="gitlab" size={32} color="#3EC6FF" />
            <Text style={styles.nameTools}>@mukhlish</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ alignItems: 'center' }}>
            <Ionicons name="logo-github" size={32} color="#3EC6FF" />
            <Text style={styles.nameTools}>@mukhlis-h</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.cardSosmed}>
        <Text style={{ fontWeight: '400', color: '#003366' }}>
          Hubungi saya
        </Text>
        <View
          style={{ borderWidth: 1, marginTop: 5, borderColor: '#8e918f' }}
        />
        <View style={styles.wraperSosmed}>
          <TouchableOpacity style={styles.sosmed}>
            <Ionicons name="logo-facebook" size={32} color="#3EC6FF" />
            <Text style={styles.nameSosmed}> mukhlis.hanafi</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.sosmed}>
            <Ionicons name="logo-instagram" size={32} color="#3EC6FF" />
            <Text style={styles.nameSosmed}> @mukhlis_hanafi</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.sosmed}>
            <Ionicons name="logo-twitter" size={32} color="#3EC6FF" />
            <Text style={styles.nameSosmed}>@mukhlish</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default AboutScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constant.statusBarHeight,
  },
  Header: {
    alignItems: 'center',
    marginTop: 30,
    height: 220,
    justifyContent: 'space-around',
    marginBottom: 10,
  },
  titleHeader: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#003366',
  },
  wraperIdentitas: {
    alignItems: 'center',
  },
  titleProfession: {
    fontSize: 12,
    color: '#3EC6FF',
    fontWeight: 'bold',
  },
  cardPortfolio: {
    backgroundColor: '#EFEFEF',
    margin: 10,
    padding: 5,
    borderRadius: 10,
    height: 125,
  },
  wraperTools: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginVertical: 15,
  },
  nameTools: {
    marginTop: 5,
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#003366',
  },
  cardSosmed: {
    backgroundColor: '#EFEFEF',
    marginHorizontal: 10,
    padding: 5,
    borderRadius: 10,
    height: 200,
  },
  wraperSosmed: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  sosmed: {
    flexDirection: 'row',
    width: 150,
    alignItems: 'center',
  },
  nameSosmed: {
    textAlign: 'left',
    marginLeft: 15,
    color: '#003366',
    fontWeight: 'bold',
    fontSize: 12,
    width: 100,
  },
});
