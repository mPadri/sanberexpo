import React, { useState, useEffect } from 'react';
import {
  View,
  Button,
  Text,
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import data from './skillData.json';
import Logo from './assets/images/logo7.png';
import { FontAwesome, MaterialIcons } from '@expo/vector-icons';
import Card from './components/Card.js';

const SkillScreen = () => {
  return (
    <View style={{ flex: 1 }}>
      <View style={styles.wrapLogo}>
        <Image source={Logo} style={{ width: '50%', marginTop: 15 }} />
      </View>

      <View style={styles.wrapHeader}>
        <MaterialIcons name="account-circle" size={28} color="#3EC6FF" />
        <View style={{ marginLeft: 10, justifyContent: 'center' }}>
          <Text style={{ fontSize: 12 }}>Hai,</Text>
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#003366' }}>
            Mukhlis Hanafi
          </Text>
        </View>
      </View>

      <View style={{ marginHorizontal: 10 }}>
        <Text style={{ fontSize: 36, color: '#003366' }}>SKILL</Text>
        <View style={{ borderWidth: 1, borderColor: '#3EC6FF' }} />
      </View>

      <View style={styles.wrapCategory}>
        <TouchableOpacity style={styles.btnCategory}>
          <Text style={styles.namaCategory}>Library/Framework</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnCategory}>
          <Text style={styles.namaCategory}>Bahasa Pemrograman</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnCategory}>
          <Text style={styles.namaCategory}>Teknologi</Text>
        </TouchableOpacity>
      </View>

      <ScrollView>
        {data.items.map((item) => {
          return (
            <TouchableOpacity key={item.id}>
              <Card
                iconName={item.iconName}
                categoryName={item.categoryName}
                percentageProgress={item.percentageProgress}
                skillName={item.skillName}
              />
              <View style={{ height: 8 }} />
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
};

export default SkillScreen;
const styles = StyleSheet.create({
  wrapLogo: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  wrapHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  wrapCategory: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginVertical: 10,
  },
  namaCategory: {
    fontSize: 12,
    color: '#003366',
  },
  btnCategory: {
    backgroundColor: '#B4E9FF',
    paddingVertical: 9,
    paddingHorizontal: 8,
    borderRadius: 8,
  },
});
