import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Constant from 'expo-constants';
import Header from './components/Header';

const LoginScreen = () => {
  return (
    <View style={styles.container}>
      <Header title="Login" />
      <View style={styles.wraperForm}>
        <Text style={styles.titleInput}>Username / Email</Text>
        <TextInput style={styles.input} />
        <View style={{ height: 15 }} />
        <Text style={styles.titleInput}>Password</Text>
        <TextInput style={styles.input} secureTextEntry />
      </View>

      <View style={styles.wraperButton}>
        <TouchableOpacity style={styles.btnMasuk}>
          <Text style={styles.titleBtn}>Masuk</Text>
        </TouchableOpacity>
        <Text style={{ color: '#3EC6FF', fontSize: 16 }}>atau</Text>
        <TouchableOpacity style={styles.btnDaftar}>
          <Text style={styles.titleBtn}>Daftar ?</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constant.statusBarHeight,
  },
  wraperForm: {
    marginVertical: 30,
    marginHorizontal: 40,
  },
  input: {
    borderWidth: 1,
    height: 40,
    padding: 5,
    borderColor: '#003366',
    borderRadius: 5,
  },
  titleInput: {
    color: '#003366',
  },
  wraperButton: {
    height: 150,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  btnMasuk: {
    backgroundColor: '#3EC6FF',
    padding: 10,
    width: 120,
    borderRadius: 20,
  },
  titleBtn: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  btnDaftar: {
    backgroundColor: '#003366',
    padding: 10,
    width: 120,
    borderRadius: 20,
  },
});
