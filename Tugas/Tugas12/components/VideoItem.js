import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

const VideoItem = ({ video }) => {
  return (
    <View style={styles.container}>
      <Image
        source={{ uri: video.snippet.thumbnails.medium.url }}
        style={{ height: 200 }}
      />

      <View style={styles.descContainer}>
        <Image
          source={{ uri: 'https://randomuser.me/api/portraits/men/0.jpg' }}
          style={{ width: 50, height: 50, borderRadius: 25 }}
        />
        <View style={styles.statusContainer}>
          <Text numberOfLines={2} style={styles.videoTitle}>
            {video.snippet.title}
          </Text>
          <Text style={styles.videoStats}>{`${
            video.snippet.channelTitle
          } · ${nFormatter(
            video.statistics.viewCount,
            1
          )} · 3 months ago`}</Text>
        </View>
        <TouchableOpacity>
          <MaterialIcons name="more-vert" size={26} color="grey" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const nFormatter = (num, digits) => {
  let si = [
    { value: 1, symbol: '' },
    { value: 1e3, symbol: 'k' },
    { value: 1e6, symbol: 'M' },
    { value: 1e9, symbol: 'G' },
    { value: 1e12, symbol: 'T' },
    { value: 1e15, symbol: 'P' },
    { value: 1e18, symbol: 'E' },
  ];

  let rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  let i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }

  return (
    (num / si[i].value).toFixed(digits).replace(rx, '$1') +
    si[i].symbol +
    ' ' +
    'views'
  );
};

export default VideoItem;

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  descContainer: {
    flexDirection: 'row',
    paddingTop: 15,
  },
  statusContainer: {
    paddingHorizontal: 15,
    flex: 1,
  },
  videoTitle: {
    fontSize: 14,
    color: '#3c3c3c',
  },
  videoStats: {
    fontSize: 12,
    paddingTop: 3,
    color: 'grey',
  },
});
