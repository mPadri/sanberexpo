import { FontAwesome, MaterialIcons } from '@expo/vector-icons';
import React, { Component } from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import VideoItem from './components/VideoItem';
import data from './data.json';
import Logo from './images/logo.png';

export default class MainApp extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image
            source={Logo}
            style={{ width: 98, height: 22, marginTop: 10 }}
          />
          <View style={styles.navBarRight}>
            <FontAwesome
              name="search"
              size={24}
              color="#212121"
              style={{ marginTop: 10 }}
            />
            <MaterialIcons
              name="account-circle"
              size={26}
              color="#212121"
              style={{ marginTop: 10 }}
            />
          </View>
        </View>

        <View style={{ flex: 1 }}>
          <FlatList
            data={data.items}
            keyExtractor={(item) => item.id}
            renderItem={(video) => <VideoItem video={video.item} />}
            ItemSeparatorComponent={() => (
              <View style={{ height: 0.5, backgroundColor: '#e5e5e5' }} />
            )}
            showsVerticalScrollIndicator={false}
          />
        </View>

        <View style={styles.tabBar}>
          <TouchableOpacity style={styles.tabItem}>
            <MaterialIcons name="home" size={26} color="grey" />
            <Text style={{ fontSize: 12, color: 'grey' }}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <MaterialIcons name="whatshot" size={26} color="grey" />
            <Text style={{ fontSize: 12, color: 'grey' }}>Trending</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <MaterialIcons name="subscriptions" size={26} color="grey" />
            <Text style={{ fontSize: 12, color: 'grey' }}>Subscribe</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <MaterialIcons name="folder" size={26} color="grey" />
            <Text style={{ fontSize: 12, color: 'grey' }}>Library</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  navBar: {
    height: 75,
    backgroundColor: 'white',
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    elevation: 3,
  },
  navBarRight: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 80,
    margin: 5,
    alignItems: 'center',
  },
  tabBar: {
    height: 55,
    backgroundColor: 'white',
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  tabItem: {
    fontSize: 12,
    alignItems: 'center',
  },
});
