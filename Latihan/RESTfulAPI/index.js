import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';

class RESTfulAPI extends Component {
  state = {
    data: [],
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch('https://randomuser.me/api?results=10');
    const json = await response.json();
    this.setState({
      data: json.results,
    });
  };
  render() {
    return (
      <View style={styles.conatiner}>
        <FlatList
          data={this.state.data}
          keyExtractor={(x, i) => i.toString()}
          renderItem={({ item }) => (
            <Text>{`${item.name.first} ${item.name.last}`}</Text>
          )}
        />

        {/* {this.state.data.map((item, i) => {
          return <Text key={i}>{`${item.name.first} ${item.name.last}`}</Text>;
        })} */}
      </View>
    );
  }
}

export default RESTfulAPI;
const styles = StyleSheet.create({
  conatiner: {
    marginTop: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
