import React from 'react';
import { View } from 'react-native';
import MainApp from './Tugas/Tugas14/App';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';
import RegisterScreen from './Tugas/Tugas13/RegisterScreen';
import LatihanRoute from './Tugas/Tugas15/LatihanRoute';
import { NavigationContainer } from '@react-navigation/native';
import Routes from './Tugas/Tugas15/Routes';
import Quiz3 from './Tugas/Quiz3';
import RESTfulAPI from './Latihan/RESTfulAPI';

export default function App() {
  return (
    <View style={{ flex: 1 }}>
      {/* <MainApp /> */}
      {/* <LoginScreen /> */}
      {/* <AboutScreen /> */}
      {/* <RegisterScreen /> */}
      {/* <MainApp /> */}
      {/* <LatihanRoute /> */}
      {/* <NavigationContainer> */}
      {/* <Routes /> */}
      {/* <LatihanRoute /> */}
      {/* </NavigationContainer> */}
      {/* <Quiz3 /> */}
      <RESTfulAPI />
    </View>
  );
}
